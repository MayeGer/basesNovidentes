﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoNoVidente
{
    public partial class frmSalud : Form
    {
        public frmSalud()
        {
            InitializeComponent();
        }

        private void btnMedico_Click(object sender, EventArgs e)
        {
            Medico s = new Medico();
            s.ShowDialog();
            this.Hide();
        }

        private void btnCertMedico_Click(object sender, EventArgs e)
        {
            CertificadoMedico s = new CertificadoMedico();
            s.ShowDialog();
            this.Hide();
        }

        private void btnSeguro_Click(object sender, EventArgs e)
        {
            Seguro s = new Seguro();
            s.ShowDialog();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MENU f = new MENU();
            f.ShowDialog();
            this.Hide();
        }
    }
}
