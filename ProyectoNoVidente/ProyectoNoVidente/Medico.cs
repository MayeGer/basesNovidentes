﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoNoVidente
{
    public partial class Medico : Form
    {
        public Medico()
        {
            InitializeComponent();
        }

        private void Medico_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'database1DataSet3.Medico' Puede moverla o quitarla según sea necesario.
            this.medicoTableAdapter.Fill(this.database1DataSet3.Medico);

        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            MENU f = new MENU();
            f.Show();
            this.Hide();
        }

        private void medicoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.medicoBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet3);

        }
    }
}
