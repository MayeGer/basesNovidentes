﻿namespace ProyectoNoVidente
{
    partial class Trabajo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label5;
            this.lbTitulo = new System.Windows.Forms.Label();
            this.txtbxCodTrab = new System.Windows.Forms.TextBox();
            this.txbxLugar = new System.Windows.Forms.TextBox();
            this.txtbxSaldo = new System.Windows.Forms.TextBox();
            this.txtbxCodNoVidente = new System.Windows.Forms.TextBox();
            this.txtbxOcup = new System.Windows.Forms.TextBox();
            this.btnModif = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbTitulo
            // 
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Bookman Old Style", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.Maroon;
            this.lbTitulo.Location = new System.Drawing.Point(415, 9);
            this.lbTitulo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(201, 42);
            this.lbTitulo.TabIndex = 4;
            this.lbTitulo.Text = "TRABAJO";
            this.lbTitulo.Click += new System.EventHandler(this.lbTitulo_Click);
            // 
            // txtbxCodTrab
            // 
            this.txtbxCodTrab.BackColor = System.Drawing.Color.GhostWhite;
            this.txtbxCodTrab.Location = new System.Drawing.Point(234, 112);
            this.txtbxCodTrab.Multiline = true;
            this.txtbxCodTrab.Name = "txtbxCodTrab";
            this.txtbxCodTrab.Size = new System.Drawing.Size(177, 35);
            this.txtbxCodTrab.TabIndex = 10;
            // 
            // txbxLugar
            // 
            this.txbxLugar.BackColor = System.Drawing.Color.GhostWhite;
            this.txbxLugar.Location = new System.Drawing.Point(234, 168);
            this.txbxLugar.Multiline = true;
            this.txbxLugar.Name = "txbxLugar";
            this.txbxLugar.Size = new System.Drawing.Size(177, 35);
            this.txbxLugar.TabIndex = 11;
            // 
            // txtbxSaldo
            // 
            this.txtbxSaldo.BackColor = System.Drawing.Color.GhostWhite;
            this.txtbxSaldo.Location = new System.Drawing.Point(234, 230);
            this.txtbxSaldo.Multiline = true;
            this.txtbxSaldo.Name = "txtbxSaldo";
            this.txtbxSaldo.Size = new System.Drawing.Size(177, 35);
            this.txtbxSaldo.TabIndex = 12;
            // 
            // txtbxCodNoVidente
            // 
            this.txtbxCodNoVidente.BackColor = System.Drawing.Color.GhostWhite;
            this.txtbxCodNoVidente.Location = new System.Drawing.Point(760, 103);
            this.txtbxCodNoVidente.Multiline = true;
            this.txtbxCodNoVidente.Name = "txtbxCodNoVidente";
            this.txtbxCodNoVidente.Size = new System.Drawing.Size(177, 35);
            this.txtbxCodNoVidente.TabIndex = 13;
            // 
            // txtbxOcup
            // 
            this.txtbxOcup.BackColor = System.Drawing.Color.GhostWhite;
            this.txtbxOcup.Location = new System.Drawing.Point(760, 168);
            this.txtbxOcup.Multiline = true;
            this.txtbxOcup.Name = "txtbxOcup";
            this.txtbxOcup.Size = new System.Drawing.Size(177, 35);
            this.txtbxOcup.TabIndex = 14;
            // 
            // btnModif
            // 
            this.btnModif.BackColor = System.Drawing.Color.DarkRed;
            this.btnModif.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModif.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModif.Location = new System.Drawing.Point(476, 243);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(151, 49);
            this.btnModif.TabIndex = 15;
            this.btnModif.Text = "Mostrar";
            this.btnModif.UseVisualStyleBackColor = false;
            this.btnModif.Click += new System.EventHandler(this.btnModif_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkRed;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(35, 432);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(128, 50);
            this.button5.TabIndex = 16;
            this.button5.Text = "Volver";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label1
            // 
            label1.BackColor = System.Drawing.Color.White;
            label1.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.Firebrick;
            label1.Location = new System.Drawing.Point(27, 114);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(175, 24);
            label1.TabIndex = 17;
            label1.Text = "CodTrabajo";
            label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            label4.BackColor = System.Drawing.Color.White;
            label4.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.ForeColor = System.Drawing.Color.Firebrick;
            label4.Location = new System.Drawing.Point(27, 167);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(175, 24);
            label4.TabIndex = 20;
            label4.Text = "Lugar";
            label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            label2.BackColor = System.Drawing.Color.White;
            label2.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.Firebrick;
            label2.Location = new System.Drawing.Point(27, 243);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(175, 24);
            label2.TabIndex = 21;
            label2.Text = "Salario";
            label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            label3.BackColor = System.Drawing.Color.White;
            label3.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.ForeColor = System.Drawing.Color.Firebrick;
            label3.Location = new System.Drawing.Point(548, 111);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(175, 24);
            label3.TabIndex = 22;
            label3.Text = "CodNoVidente";
            label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            label5.BackColor = System.Drawing.Color.White;
            label5.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.ForeColor = System.Drawing.Color.Firebrick;
            label5.Location = new System.Drawing.Point(539, 179);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(175, 24);
            label5.TabIndex = 23;
            label5.Text = "Ocupacion";
            label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Trabajo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoNoVidente.Properties.Resources.back_to_work_wallpaper_1366x768;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1041, 494);
            this.Controls.Add(label5);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label4);
            this.Controls.Add(label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.txtbxOcup);
            this.Controls.Add(this.txtbxCodNoVidente);
            this.Controls.Add(this.txtbxSaldo);
            this.Controls.Add(this.txbxLugar);
            this.Controls.Add(this.txtbxCodTrab);
            this.Controls.Add(this.lbTitulo);
            this.Font = new System.Drawing.Font("Bookman Old Style", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Trabajo";
            this.Text = "Trabajo";
            this.Load += new System.EventHandler(this.Trabajo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.TextBox txtbxCodTrab;
        private System.Windows.Forms.TextBox txbxLugar;
        private System.Windows.Forms.TextBox txtbxSaldo;
        private System.Windows.Forms.TextBox txtbxCodNoVidente;
        private System.Windows.Forms.TextBox txtbxOcup;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.Button button5;
    }
}