﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoNoVidente
{
    public partial class EducacionFormal : Form
    {
        public EducacionFormal()
        {
            InitializeComponent();
        }

        private void btnReha_Click(object sender, EventArgs e)
        {
            Instituto s = new Instituto();
            s.ShowDialog();
            this.Close();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            MENU f = new MENU();
            f.ShowDialog();
            this.Close();
        }
    }
}
