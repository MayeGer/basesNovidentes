﻿namespace ProyectoNoVidente
{
    partial class Instituto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label codNoVidenteLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            this.lbTitulo = new System.Windows.Forms.Label();
            this.txtCodNoVid = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnModif = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            codNoVidenteLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbTitulo
            // 
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Bookman Old Style", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.Chocolate;
            this.lbTitulo.Location = new System.Drawing.Point(310, 27);
            this.lbTitulo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(254, 47);
            this.lbTitulo.TabIndex = 6;
            this.lbTitulo.Text = "INSTITUTO";
            // 
            // txtCodNoVid
            // 
            this.txtCodNoVid.BackColor = System.Drawing.Color.SaddleBrown;
            this.txtCodNoVid.Location = new System.Drawing.Point(436, 128);
            this.txtCodNoVid.Multiline = true;
            this.txtCodNoVid.Name = "txtCodNoVid";
            this.txtCodNoVid.Size = new System.Drawing.Size(201, 35);
            this.txtCodNoVid.TabIndex = 11;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.SaddleBrown;
            this.textBox1.Location = new System.Drawing.Point(436, 224);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(201, 35);
            this.textBox1.TabIndex = 12;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.SaddleBrown;
            this.textBox2.Location = new System.Drawing.Point(436, 325);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(201, 35);
            this.textBox2.TabIndex = 13;
            // 
            // btnModif
            // 
            this.btnModif.BackColor = System.Drawing.Color.Sienna;
            this.btnModif.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModif.ForeColor = System.Drawing.Color.DarkOrange;
            this.btnModif.Location = new System.Drawing.Point(264, 415);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(133, 49);
            this.btnModif.TabIndex = 14;
            this.btnModif.Text = "Mostrar";
            this.btnModif.UseVisualStyleBackColor = false;
            this.btnModif.Click += new System.EventHandler(this.btnModif_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Sienna;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.DarkOrange;
            this.button4.Location = new System.Drawing.Point(515, 415);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(133, 49);
            this.button4.TabIndex = 15;
            this.button4.Text = "Volver";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // codNoVidenteLabel
            // 
            codNoVidenteLabel.BackColor = System.Drawing.Color.Peru;
            codNoVidenteLabel.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            codNoVidenteLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            codNoVidenteLabel.Location = new System.Drawing.Point(165, 128);
            codNoVidenteLabel.Name = "codNoVidenteLabel";
            codNoVidenteLabel.Size = new System.Drawing.Size(203, 35);
            codNoVidenteLabel.TabIndex = 16;
            codNoVidenteLabel.Text = "CodInsitituto";
            codNoVidenteLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            codNoVidenteLabel.Click += new System.EventHandler(this.codNoVidenteLabel_Click);
            // 
            // label1
            // 
            label1.BackColor = System.Drawing.Color.Peru;
            label1.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            label1.Location = new System.Drawing.Point(161, 224);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(219, 35);
            label1.TabIndex = 17;
            label1.Text = "NombreInstituto";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            label2.BackColor = System.Drawing.Color.Peru;
            label2.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            label2.Location = new System.Drawing.Point(157, 320);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(223, 35);
            label2.TabIndex = 18;
            label2.Text = "Catergoria";
            label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Instituto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoNoVidente.Properties.Resources.wallpapersxl_fondos_vintage_hd_de_pantalla_deluxe_originales_bonitas_280063_1400x1050;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(956, 491);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(codNoVidenteLabel);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtCodNoVid);
            this.Controls.Add(this.lbTitulo);
            this.Name = "Instituto";
            this.Text = "Instituto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.TextBox txtCodNoVid;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.Button button4;
    }
}