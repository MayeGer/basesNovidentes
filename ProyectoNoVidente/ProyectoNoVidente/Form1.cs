﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoNoVidente
{
    public partial class MENU : Form
    {
        public MENU()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnNoVidente_Click(object sender, EventArgs e)
        {
            frmNoVidente s = new frmNoVidente();
            s.Show();
            this.Hide();
        }

        private void btnSalud_Click(object sender, EventArgs e)
        {
            frmSalud s = new frmSalud();
            s.Show();
            this.Hide();
        }

        private void btnTrabajo_Click(object sender, EventArgs e)
        {
            Trabajo s = new Trabajo();
            s.Show();
            this.Hide();
        }

        private void btnEducacion_Click(object sender, EventArgs e)
        {
            EducacionFormal s = new EducacionFormal();
            s.Show();
            this.Hide();
        }

        private void btnReha_Click(object sender, EventArgs e)
        {
            Rehabilitacion s = new Rehabilitacion();
            s.Show();
            this.Hide();
        }

        private void btnHogar_Click(object sender, EventArgs e)
        {
            Hogar s = new Hogar();
            s.Show();
            this.Hide();
        }
    }
}
