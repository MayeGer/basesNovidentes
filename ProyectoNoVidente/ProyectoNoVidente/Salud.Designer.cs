﻿namespace ProyectoNoVidente
{
    partial class frmSalud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMedico = new System.Windows.Forms.Button();
            this.btnCertMedico = new System.Windows.Forms.Button();
            this.btnSeguro = new System.Windows.Forms.Button();
            this.lbSalud = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMedico
            // 
            this.btnMedico.BackColor = System.Drawing.Color.Black;
            this.btnMedico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMedico.Font = new System.Drawing.Font("Bookman Old Style", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMedico.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnMedico.Location = new System.Drawing.Point(299, 133);
            this.btnMedico.Name = "btnMedico";
            this.btnMedico.Size = new System.Drawing.Size(166, 44);
            this.btnMedico.TabIndex = 1;
            this.btnMedico.Text = "Medico";
            this.btnMedico.UseVisualStyleBackColor = false;
            this.btnMedico.Click += new System.EventHandler(this.btnMedico_Click);
            // 
            // btnCertMedico
            // 
            this.btnCertMedico.BackColor = System.Drawing.Color.Black;
            this.btnCertMedico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCertMedico.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCertMedico.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnCertMedico.Location = new System.Drawing.Point(268, 221);
            this.btnCertMedico.Name = "btnCertMedico";
            this.btnCertMedico.Size = new System.Drawing.Size(237, 45);
            this.btnCertMedico.TabIndex = 2;
            this.btnCertMedico.Text = "Certificado Medico";
            this.btnCertMedico.UseVisualStyleBackColor = false;
            this.btnCertMedico.Click += new System.EventHandler(this.btnCertMedico_Click);
            // 
            // btnSeguro
            // 
            this.btnSeguro.BackColor = System.Drawing.Color.Black;
            this.btnSeguro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSeguro.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeguro.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.btnSeguro.Location = new System.Drawing.Point(316, 305);
            this.btnSeguro.Name = "btnSeguro";
            this.btnSeguro.Size = new System.Drawing.Size(149, 37);
            this.btnSeguro.TabIndex = 3;
            this.btnSeguro.Text = "Seguro";
            this.btnSeguro.UseVisualStyleBackColor = false;
            this.btnSeguro.Click += new System.EventHandler(this.btnSeguro_Click);
            // 
            // lbSalud
            // 
            this.lbSalud.AutoSize = true;
            this.lbSalud.BackColor = System.Drawing.Color.Transparent;
            this.lbSalud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbSalud.Font = new System.Drawing.Font("Elephant", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSalud.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lbSalud.Location = new System.Drawing.Point(308, 19);
            this.lbSalud.Name = "lbSalud";
            this.lbSalud.Size = new System.Drawing.Size(142, 51);
            this.lbSalud.TabIndex = 4;
            this.lbSalud.Text = "Salud";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.button1.Location = new System.Drawing.Point(317, 390);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(149, 37);
            this.button1.TabIndex = 5;
            this.button1.Text = "Volver";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmSalud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoNoVidente.Properties.Resources.images;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(770, 459);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbSalud);
            this.Controls.Add(this.btnSeguro);
            this.Controls.Add(this.btnCertMedico);
            this.Controls.Add(this.btnMedico);
            this.Name = "frmSalud";
            this.Text = "Salud";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMedico;
        private System.Windows.Forms.Button btnCertMedico;
        private System.Windows.Forms.Button btnSeguro;
        private System.Windows.Forms.Label lbSalud;
        private System.Windows.Forms.Button button1;
    }
}