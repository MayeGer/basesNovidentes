﻿namespace ProyectoNoVidente
{
    partial class MENU
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNoVidente = new System.Windows.Forms.Button();
            this.btnSalud = new System.Windows.Forms.Button();
            this.btnTrabajo = new System.Windows.Forms.Button();
            this.lbTitulo = new System.Windows.Forms.Label();
            this.btnReha = new System.Windows.Forms.Button();
            this.btnEducacion = new System.Windows.Forms.Button();
            this.btnHogar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNoVidente
            // 
            this.btnNoVidente.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnNoVidente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNoVidente.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoVidente.ForeColor = System.Drawing.Color.Black;
            this.btnNoVidente.Location = new System.Drawing.Point(388, 172);
            this.btnNoVidente.Name = "btnNoVidente";
            this.btnNoVidente.Size = new System.Drawing.Size(165, 38);
            this.btnNoVidente.TabIndex = 0;
            this.btnNoVidente.Text = "NO VIDENTE";
            this.btnNoVidente.UseVisualStyleBackColor = false;
            this.btnNoVidente.Click += new System.EventHandler(this.btnNoVidente_Click);
            // 
            // btnSalud
            // 
            this.btnSalud.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnSalud.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSalud.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalud.ForeColor = System.Drawing.Color.Black;
            this.btnSalud.Location = new System.Drawing.Point(429, 408);
            this.btnSalud.Name = "btnSalud";
            this.btnSalud.Size = new System.Drawing.Size(147, 37);
            this.btnSalud.TabIndex = 1;
            this.btnSalud.Text = "Salud";
            this.btnSalud.UseVisualStyleBackColor = false;
            this.btnSalud.Click += new System.EventHandler(this.btnSalud_Click);
            // 
            // btnTrabajo
            // 
            this.btnTrabajo.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnTrabajo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTrabajo.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrabajo.ForeColor = System.Drawing.Color.Black;
            this.btnTrabajo.Location = new System.Drawing.Point(146, 408);
            this.btnTrabajo.Name = "btnTrabajo";
            this.btnTrabajo.Size = new System.Drawing.Size(155, 37);
            this.btnTrabajo.TabIndex = 2;
            this.btnTrabajo.Text = "Trabajo";
            this.btnTrabajo.UseVisualStyleBackColor = false;
            this.btnTrabajo.Click += new System.EventHandler(this.btnTrabajo_Click);
            // 
            // lbTitulo
            // 
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Castellar", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.lbTitulo.Location = new System.Drawing.Point(56, 22);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(622, 39);
            this.lbTitulo.TabIndex = 3;
            this.lbTitulo.Text = "Informacion del No Vidente";
            // 
            // btnReha
            // 
            this.btnReha.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnReha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReha.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReha.ForeColor = System.Drawing.Color.Black;
            this.btnReha.Location = new System.Drawing.Point(87, 263);
            this.btnReha.Name = "btnReha";
            this.btnReha.Size = new System.Drawing.Size(155, 37);
            this.btnReha.TabIndex = 5;
            this.btnReha.Text = "Rehabilitacion";
            this.btnReha.UseVisualStyleBackColor = false;
            this.btnReha.Click += new System.EventHandler(this.btnReha_Click);
            // 
            // btnEducacion
            // 
            this.btnEducacion.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnEducacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEducacion.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEducacion.ForeColor = System.Drawing.Color.Black;
            this.btnEducacion.Location = new System.Drawing.Point(714, 250);
            this.btnEducacion.Name = "btnEducacion";
            this.btnEducacion.Size = new System.Drawing.Size(206, 38);
            this.btnEducacion.TabIndex = 6;
            this.btnEducacion.Text = "Educacion Formal";
            this.btnEducacion.UseVisualStyleBackColor = false;
            this.btnEducacion.Click += new System.EventHandler(this.btnEducacion_Click);
            // 
            // btnHogar
            // 
            this.btnHogar.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnHogar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHogar.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHogar.ForeColor = System.Drawing.Color.Black;
            this.btnHogar.Location = new System.Drawing.Point(690, 408);
            this.btnHogar.Name = "btnHogar";
            this.btnHogar.Size = new System.Drawing.Size(155, 38);
            this.btnHogar.TabIndex = 7;
            this.btnHogar.Text = "Hogar";
            this.btnHogar.UseVisualStyleBackColor = false;
            this.btnHogar.Click += new System.EventHandler(this.btnHogar_Click);
            // 
            // MENU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.BlueViolet;
            this.BackgroundImage = global::ProyectoNoVidente.Properties.Resources.tree_pink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1031, 504);
            this.Controls.Add(this.btnHogar);
            this.Controls.Add(this.btnEducacion);
            this.Controls.Add(this.btnReha);
            this.Controls.Add(this.lbTitulo);
            this.Controls.Add(this.btnTrabajo);
            this.Controls.Add(this.btnSalud);
            this.Controls.Add(this.btnNoVidente);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.Name = "MENU";
            this.Text = "MENU";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNoVidente;
        private System.Windows.Forms.Button btnSalud;
        private System.Windows.Forms.Button btnTrabajo;
        private System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.Button btnReha;
        private System.Windows.Forms.Button btnEducacion;
        private System.Windows.Forms.Button btnHogar;
    }
}

