﻿namespace ProyectoNoVidente
{
    partial class Vivienda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            this.lbA = new System.Windows.Forms.Label();
            this.txtCodNoVid = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbxMatecho = new System.Windows.Forms.ComboBox();
            this.cmbxMatPared = new System.Windows.Forms.ComboBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = System.Drawing.Color.Transparent;
            label1.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.LightSkyBlue;
            label1.Location = new System.Drawing.Point(178, 109);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(138, 24);
            label1.TabIndex = 10;
            label1.Text = "CodVivienda";
            label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = System.Drawing.Color.Transparent;
            label2.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            label2.Location = new System.Drawing.Point(255, 166);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(61, 24);
            label2.TabIndex = 11;
            label2.Text = "Zona";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = System.Drawing.Color.Transparent;
            label3.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            label3.Location = new System.Drawing.Point(206, 223);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(110, 24);
            label3.TabIndex = 12;
            label3.Text = "Propiedad";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = System.Drawing.Color.Transparent;
            label4.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            label4.Location = new System.Drawing.Point(195, 279);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(121, 24);
            label4.TabIndex = 13;
            label4.Text = "Alumbrado";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = System.Drawing.Color.Transparent;
            label5.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.ForeColor = System.Drawing.Color.LightSkyBlue;
            label5.Location = new System.Drawing.Point(195, 333);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(112, 24);
            label5.TabIndex = 14;
            label5.Text = "MatTecho";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = System.Drawing.Color.Transparent;
            label6.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.ForeColor = System.Drawing.Color.LightSkyBlue;
            label6.Location = new System.Drawing.Point(195, 387);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(106, 24);
            label6.TabIndex = 15;
            label6.Text = "MatPared";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.BackColor = System.Drawing.Color.Transparent;
            label7.Font = new System.Drawing.Font("Bookman Old Style", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.ForeColor = System.Drawing.Color.LightSkyBlue;
            label7.Location = new System.Drawing.Point(163, 444);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(145, 24);
            label7.TabIndex = 16;
            label7.Text = "Accesibilidad";
            // 
            // lbA
            // 
            this.lbA.AutoSize = true;
            this.lbA.BackColor = System.Drawing.Color.Transparent;
            this.lbA.Font = new System.Drawing.Font("Bookman Old Style", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbA.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.lbA.Location = new System.Drawing.Point(272, 9);
            this.lbA.Name = "lbA";
            this.lbA.Size = new System.Drawing.Size(229, 47);
            this.lbA.TabIndex = 9;
            this.lbA.Text = "VIVIENDA";
            // 
            // txtCodNoVid
            // 
            this.txtCodNoVid.BackColor = System.Drawing.Color.LightBlue;
            this.txtCodNoVid.Location = new System.Drawing.Point(338, 105);
            this.txtCodNoVid.Name = "txtCodNoVid";
            this.txtCodNoVid.Size = new System.Drawing.Size(314, 26);
            this.txtCodNoVid.TabIndex = 18;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.LightBlue;
            this.textBox1.Location = new System.Drawing.Point(338, 161);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(314, 26);
            this.textBox1.TabIndex = 19;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.LightBlue;
            this.textBox2.Location = new System.Drawing.Point(338, 223);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(314, 26);
            this.textBox2.TabIndex = 20;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.LightBlue;
            this.textBox3.Location = new System.Drawing.Point(336, 279);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(314, 26);
            this.textBox3.TabIndex = 21;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.LightBlue;
            this.textBox6.Location = new System.Drawing.Point(336, 444);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(314, 26);
            this.textBox6.TabIndex = 24;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnRegistrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnRegistrar.Location = new System.Drawing.Point(229, 504);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(148, 49);
            this.btnRegistrar.TabIndex = 17;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(451, 504);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 49);
            this.button1.TabIndex = 25;
            this.button1.Text = "Volver";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbxMatecho
            // 
            this.cmbxMatecho.FormattingEnabled = true;
            this.cmbxMatecho.Items.AddRange(new object[] {
            "Paja",
            "Cemento",
            "Calamina"});
            this.cmbxMatecho.Location = new System.Drawing.Point(368, 329);
            this.cmbxMatecho.Name = "cmbxMatecho";
            this.cmbxMatecho.Size = new System.Drawing.Size(121, 28);
            this.cmbxMatecho.TabIndex = 26;
            // 
            // cmbxMatPared
            // 
            this.cmbxMatPared.FormattingEnabled = true;
            this.cmbxMatPared.Items.AddRange(new object[] {
            "Adobe",
            "Cemento",
            "Ladrillo"});
            this.cmbxMatPared.Location = new System.Drawing.Point(368, 383);
            this.cmbxMatPared.Name = "cmbxMatPared";
            this.cmbxMatPared.Size = new System.Drawing.Size(121, 28);
            this.cmbxMatPared.TabIndex = 27;
            // 
            // Vivienda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoNoVidente.Properties.Resources._1920x1080_lake_nature_summer_tree_forest_splendid_lake_house_6219;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(856, 584);
            this.Controls.Add(this.cmbxMatPared);
            this.Controls.Add(this.cmbxMatecho);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtCodNoVid);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(label7);
            this.Controls.Add(label6);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.lbA);
            this.Name = "Vivienda";
            this.Text = "Vivienda";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbA;
        private System.Windows.Forms.TextBox txtCodNoVid;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbxMatecho;
        private System.Windows.Forms.ComboBox cmbxMatPared;
    }
}