﻿namespace ProyectoNoVidente
{
    partial class CertificadoMedico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label codNoVidenteLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            this.lbTitulo = new System.Windows.Forms.Label();
            this.codNoVidenteTextBox = new System.Windows.Forms.TextBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            codNoVidenteLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbTitulo
            // 
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Bookman Old Style", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.IndianRed;
            this.lbTitulo.Location = new System.Drawing.Point(182, 9);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(402, 28);
            this.lbTitulo.TabIndex = 4;
            this.lbTitulo.Text = "DATOS CERTIFICADO MEDICO";
            this.lbTitulo.Click += new System.EventHandler(this.lbTitulo_Click);
            // 
            // codNoVidenteTextBox
            // 
            this.codNoVidenteTextBox.BackColor = System.Drawing.Color.MistyRose;
            this.codNoVidenteTextBox.Location = new System.Drawing.Point(255, 67);
            this.codNoVidenteTextBox.Multiline = true;
            this.codNoVidenteTextBox.Name = "codNoVidenteTextBox";
            this.codNoVidenteTextBox.Size = new System.Drawing.Size(260, 36);
            this.codNoVidenteTextBox.TabIndex = 7;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.BackColor = System.Drawing.Color.LightCoral;
            this.btnRegistrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRegistrar.Location = new System.Drawing.Point(170, 412);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(136, 42);
            this.btnRegistrar.TabIndex = 14;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LightCoral;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(365, 412);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 42);
            this.button2.TabIndex = 15;
            this.button2.Text = "Volver";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.MistyRose;
            this.textBox1.Location = new System.Drawing.Point(255, 115);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(260, 36);
            this.textBox1.TabIndex = 20;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.MistyRose;
            this.textBox2.Location = new System.Drawing.Point(255, 163);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(260, 36);
            this.textBox2.TabIndex = 21;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.MistyRose;
            this.textBox3.Location = new System.Drawing.Point(255, 208);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(364, 36);
            this.textBox3.TabIndex = 22;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.MistyRose;
            this.textBox4.Location = new System.Drawing.Point(255, 263);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(364, 36);
            this.textBox4.TabIndex = 23;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.MistyRose;
            this.textBox5.Location = new System.Drawing.Point(255, 318);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(364, 36);
            this.textBox5.TabIndex = 24;
            // 
            // codNoVidenteLabel
            // 
            codNoVidenteLabel.BackColor = System.Drawing.Color.White;
            codNoVidenteLabel.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            codNoVidenteLabel.ForeColor = System.Drawing.Color.IndianRed;
            codNoVidenteLabel.Location = new System.Drawing.Point(59, 61);
            codNoVidenteLabel.Name = "codNoVidenteLabel";
            codNoVidenteLabel.Size = new System.Drawing.Size(182, 36);
            codNoVidenteLabel.TabIndex = 25;
            codNoVidenteLabel.Text = "Cod No Vidente:";
            codNoVidenteLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            codNoVidenteLabel.UseMnemonic = false;
            // 
            // label1
            // 
            label1.BackColor = System.Drawing.Color.White;
            label1.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.IndianRed;
            label1.Location = new System.Drawing.Point(59, 109);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(182, 36);
            label1.TabIndex = 26;
            label1.Text = "Cod Medico:";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            label1.UseMnemonic = false;
            // 
            // label2
            // 
            label2.BackColor = System.Drawing.Color.White;
            label2.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.IndianRed;
            label2.Location = new System.Drawing.Point(63, 163);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(182, 36);
            label2.TabIndex = 27;
            label2.Text = "Cod No Vidente:";
            label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            label2.UseMnemonic = false;
            // 
            // label3
            // 
            label3.BackColor = System.Drawing.Color.White;
            label3.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.ForeColor = System.Drawing.Color.IndianRed;
            label3.Location = new System.Drawing.Point(43, 208);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(198, 36);
            label3.TabIndex = 28;
            label3.Text = "Tipo Discapacidad:";
            label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            label3.UseMnemonic = false;
            // 
            // label4
            // 
            label4.BackColor = System.Drawing.Color.White;
            label4.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.ForeColor = System.Drawing.Color.IndianRed;
            label4.Location = new System.Drawing.Point(59, 263);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(182, 36);
            label4.TabIndex = 29;
            label4.Text = "Terapia:";
            label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            label4.UseMnemonic = false;
            // 
            // label5
            // 
            label5.BackColor = System.Drawing.Color.White;
            label5.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.ForeColor = System.Drawing.Color.IndianRed;
            label5.Location = new System.Drawing.Point(59, 312);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(182, 36);
            label5.TabIndex = 30;
            label5.Text = "Diagnostico:";
            label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            label5.UseMnemonic = false;
            // 
            // CertificadoMedico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoNoVidente.Properties.Resources._7;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(788, 485);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(codNoVidenteLabel);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.codNoVidenteTextBox);
            this.Controls.Add(this.lbTitulo);
            this.Name = "CertificadoMedico";
            this.Text = "CertificadoMedico";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.TextBox codNoVidenteTextBox;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
    }
}