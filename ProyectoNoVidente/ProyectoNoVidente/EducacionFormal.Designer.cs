﻿namespace ProyectoNoVidente
{
    partial class EducacionFormal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label codNoVidenteLabel;
            System.Windows.Forms.Label label1;
            this.lbTitulo = new System.Windows.Forms.Label();
            this.txtCodNoVid = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnModif = new System.Windows.Forms.Button();
            this.btnReha = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            codNoVidenteLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbTitulo
            // 
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Bookman Old Style", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbTitulo.Location = new System.Drawing.Point(236, 9);
            this.lbTitulo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(472, 47);
            this.lbTitulo.TabIndex = 5;
            this.lbTitulo.Text = "EDUCACION FORMAL";
            // 
            // txtCodNoVid
            // 
            this.txtCodNoVid.BackColor = System.Drawing.Color.Linen;
            this.txtCodNoVid.Location = new System.Drawing.Point(459, 86);
            this.txtCodNoVid.Multiline = true;
            this.txtCodNoVid.Name = "txtCodNoVid";
            this.txtCodNoVid.Size = new System.Drawing.Size(230, 35);
            this.txtCodNoVid.TabIndex = 10;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Linen;
            this.textBox1.Location = new System.Drawing.Point(459, 162);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(230, 35);
            this.textBox1.TabIndex = 11;
            // 
            // btnModif
            // 
            this.btnModif.BackColor = System.Drawing.Color.Sienna;
            this.btnModif.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModif.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModif.Location = new System.Drawing.Point(517, 244);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(133, 49);
            this.btnModif.TabIndex = 13;
            this.btnModif.Text = "Mostrar";
            this.btnModif.UseVisualStyleBackColor = false;
            // 
            // btnReha
            // 
            this.btnReha.BackColor = System.Drawing.Color.Sienna;
            this.btnReha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReha.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReha.ForeColor = System.Drawing.Color.White;
            this.btnReha.Location = new System.Drawing.Point(46, 351);
            this.btnReha.Name = "btnReha";
            this.btnReha.Size = new System.Drawing.Size(155, 37);
            this.btnReha.TabIndex = 14;
            this.btnReha.Text = "Instituto";
            this.btnReha.UseVisualStyleBackColor = false;
            this.btnReha.Click += new System.EventHandler(this.btnReha_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.BackColor = System.Drawing.Color.Sienna;
            this.btnVolver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVolver.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVolver.ForeColor = System.Drawing.Color.White;
            this.btnVolver.Location = new System.Drawing.Point(46, 438);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(155, 37);
            this.btnVolver.TabIndex = 15;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = false;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // codNoVidenteLabel
            // 
            codNoVidenteLabel.AutoSize = true;
            codNoVidenteLabel.BackColor = System.Drawing.Color.Transparent;
            codNoVidenteLabel.Font = new System.Drawing.Font("Bookman Old Style", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            codNoVidenteLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            codNoVidenteLabel.Location = new System.Drawing.Point(250, 96);
            codNoVidenteLabel.Name = "codNoVidenteLabel";
            codNoVidenteLabel.Size = new System.Drawing.Size(177, 25);
            codNoVidenteLabel.TabIndex = 16;
            codNoVidenteLabel.Text = "CodEducacion";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = System.Drawing.Color.Transparent;
            label1.Font = new System.Drawing.Font("Bookman Old Style", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            label1.Location = new System.Drawing.Point(358, 172);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(69, 25);
            label1.TabIndex = 17;
            label1.Text = "Nivel";
            // 
            // EducacionFormal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProyectoNoVidente.Properties.Resources.cropped_free_hd_wallpapers_1080p_2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1040, 520);
            this.Controls.Add(label1);
            this.Controls.Add(codNoVidenteLabel);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnReha);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtCodNoVid);
            this.Controls.Add(this.lbTitulo);
            this.Name = "EducacionFormal";
            this.Text = "EducacionFormal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.TextBox txtCodNoVid;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.Button btnReha;
        private System.Windows.Forms.Button btnVolver;
    }
}