﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoNoVidente
{
    public partial class Hogar : Form
    {
        public Hogar()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MENU f = new MENU();
            f.Show();
            this.Hide();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Vivienda s = new Vivienda();
            s.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MunProDep s = new MunProDep();
            s.Show();
            this.Hide();
        }
    }
}
